# sasw-test-support
Opinionated template for practicing TDD and write any unit, functional or integration test following a Given-When-Then approach with xUnit framework.

The goal is to make tests more readable, in a standarized and consistent way, and to promote a deterministic and idempotent approach by encouraging
the configuration of preconditions and the execution outside the fact assertions.

It targets `netstandard2.1` (it can be used with any version of .NET Core and .NET 5+)

It supports async Given or both Given and When. The mode can be selected depending on the base class to extend:
- `Given_When_Then_Test`
- `Given_WhenAsync_Then_Test`
- `GivenAsync_WhenAsync_Then_Test`

Simply add class in your xUnit project extending either of these base clases and implement your scenarios. Example:

```
namespace Sasw.Sample
{
    public static class GetAsTextTests
    {
        public class Given_A_Number_When_Getting_As_Text
            : Given_When_Then_Test
        {
            private MyClass _sut = null!;
            private int _inputNumber;
            private string _result = null!;

            protected override void Given()
            {
                _sut = new MyClass();
                _inputNumber = 123;
                _expectedText = "123";
            }

            protected override void When()
            {
                _result = _sut.GetAsText(_inputNumber)
            }

            [Fact]
            public void Then_It_Should_Return_Valid_Text()
            {
                // NOTE: this sample assumes you're using FluentAssertions or similar.
                _result.ShouldBe(_expectedText);
            }

            // other facts for the same scenario here.
        }
    }
}
```