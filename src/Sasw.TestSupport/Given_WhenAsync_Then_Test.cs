﻿using System.Threading.Tasks;
using Xunit;

namespace Sasw.TestSupport
{
    public abstract class Given_WhenAsync_Then_Test
        : IAsyncLifetime
    {
        protected Given_WhenAsync_Then_Test()
        {
            Setup();
        }

        public async Task InitializeAsync()
        {
            await When();
        }

        public async Task DisposeAsync()
        {
            await Cleanup();
        }

        protected virtual Task Cleanup()
        {
            return Task.CompletedTask;
        }

        protected abstract void Given();

        protected abstract Task When();

        private void Setup()
        {
            Given();
        }
    }
}